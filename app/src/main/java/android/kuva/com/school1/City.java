package android.kuva.com.school1;

import java.util.ArrayList;

/**
 * Created by KuVa on 21.07.2015.
 */
public class City
{
    private String name;
    private String description;

    private static ArrayList<City> cities;

    public City(String name, String description)
    {
        this.name = name;
        this.description = description;
    }

    public static ArrayList<City> getCities()
    {
        if (cities == null)
            innitiate();
        return cities;
    }


    private static void innitiate()
    {
        cities = new ArrayList<>();
        cities.add(new City("Saint-Petersburg", "North capital"));
        cities.add(new City("Prague", "Beer capital"));
        cities.add(new City("Sochi", "Swim, relax"));
        cities.add(new City("Amsterdam", "Smoke, relax"));
    }

    public static City getCity(int id)
    {
        return cities.get(id);
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }


}
