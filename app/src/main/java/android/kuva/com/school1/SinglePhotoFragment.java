package android.kuva.com.school1;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;


import java.io.IOException;
import java.io.InputStream;

/**
 * Created by KuVa on 23.07.2015.
 */
public class SinglePhotoFragment extends Fragment
{
    public static final String PATH = "PATH";

    private String path;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        path = getArguments().getString(PATH);


    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.single_photo, container, false);

        ImageView imageView = (ImageView) view.findViewById(R.id.singlePhoto);
        try
        {
            InputStream inputStream = getActivity().getAssets().open(path);
            Drawable d = Drawable.createFromStream(inputStream, null);

            imageView.setImageDrawable(d);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }


        return view;
    }



}
