package android.kuva.com.school1;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener
{

    private ListView listView;
    private TextView cityName;
    private TextView cityDescription;
    private Button showPhotos;

    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.citiesListView);
        CitiesAdapter adapter = new CitiesAdapter(City.getCities());
        listView.setAdapter(adapter);

        cityName = (TextView) findViewById(R.id.cityNameTextView);
        cityDescription = (TextView) findViewById(R.id.cityDescriptionTextView);
        showPhotos = (Button) findViewById(R.id.showPhotosButton);
        showPhotos.setVisibility(View.INVISIBLE);

        showPhotos.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(MainActivity.this, PhotosActivity.class);
                intent.putExtra(PhotosActivity.CITY, position);
                startActivity(intent);
            }
        });

        listView.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        City city = (City) parent.getItemAtPosition(position);
        cityName.setText(city.getName());
        cityDescription.setText(city.getDescription());
        showPhotos.setVisibility(View.VISIBLE);
        this.position = position;
    }

    private class CitiesAdapter extends ArrayAdapter<City>
    {
        public CitiesAdapter(ArrayList<City> cities)
        {
            super(MainActivity.this, 0, cities);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
                convertView = MainActivity.this.getLayoutInflater().inflate(R.layout.list_item_city, null);

            City city = getItem(position);
            TextView cityTextView = (TextView) convertView.findViewById(R.id.citiesTextView);
            cityTextView.setText(city.getName());

            return convertView;
        }
    }


}
