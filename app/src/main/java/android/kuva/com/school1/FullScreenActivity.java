package android.kuva.com.school1;


import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import android.support.v4.app.FragmentStatePagerAdapter;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.File;
import java.io.IOException;

/**
 * Created by KuVa on 23.07.2015.
 */
public class FullScreenActivity extends AppCompatActivity
{
    public static final String CITY_NAME = "CITY_NAME";
    public static final String PHOTO_POSITION = "PHOTO_POSITION";

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);

        viewPager = (ViewPager) findViewById(R.id.fullscreen_viewPager);

        String cityName = getIntent().getStringExtra(CITY_NAME);
        int position = getIntent().getIntExtra(PHOTO_POSITION, 0);

        FragmentManager fm = getSupportFragmentManager();
        viewPager.setAdapter(new ImagesPagerAdapter(fm, cityName));
        viewPager.setCurrentItem(position);

    }

    public class ImagesPagerAdapter extends FragmentStatePagerAdapter
    {
        private String list[];
        private String cityName;

        public ImagesPagerAdapter(FragmentManager fm, String cityName)
        {
            super(fm);
            AssetManager manager = getAssets();
            this.cityName = cityName;
            try
            {
                list = manager.list(cityName);

                for (int i = 0; i < list.length; i++)
                    Log.d("Photos", list[i]);
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }


        @Override
        public Fragment getItem(int position)
        {
            Bundle args = new Bundle();
            args.putString(SinglePhotoFragment.PATH, cityName + File.separator + list[position]);

            SinglePhotoFragment fragment = new SinglePhotoFragment();
            fragment.setArguments(args);

            return fragment;
        }

        @Override
        public int getCount()
        {
            return list.length;
        }
    }
}
