package android.kuva.com.school1;


import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by KuVa on 21.07.2015.
 */
public class PhotosActivity extends AppCompatActivity
{
    public static final String CITY = "CITY_PHOTO";

    private GridView gridView;
    private City city;




    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);

        city = City.getCity(getIntent().getIntExtra(CITY, 0));
        gridView = (GridView) findViewById(R.id.photosGridView);
        PhotosAdapter adapter = new PhotosAdapter(this);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent intent = new Intent(PhotosActivity.this, FullScreenActivity.class);
                intent.putExtra(FullScreenActivity.CITY_NAME, city.getName());
                intent.putExtra(FullScreenActivity.PHOTO_POSITION, position);
                startActivity(intent);
            }
        });



    }

    public class PhotosAdapter extends BaseAdapter
    {

        private Context context;
        private String[] list;

        public PhotosAdapter (Context context)
        {
            this.context = context;
            AssetManager manager = context.getAssets();
            try
            {
                list = manager.list(city.getName());

                for (int i = 0; i < list.length; i++)
                    Log.d("Photos" , list[i]);
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }


        @Override
        public int getCount()
        {
            return list.length;
        }

        @Override
        public Object getItem(int position)
        {
            return list[position];
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ImageView imageView = new ImageView(context);
            try
            {
                InputStream inputStream = getAssets().open(city.getName() + File.separator + list[position]);
                Drawable d = Drawable.createFromStream(inputStream, null);

                imageView.setImageDrawable(d);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setLayoutParams(new GridView.LayoutParams(340, 300));

            imageView.startAnimation(AnimationUtils.loadAnimation(PhotosActivity.this, R.anim.test));

            return imageView;
        }
    }
}
